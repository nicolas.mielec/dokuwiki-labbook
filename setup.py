from distutils.core import setup

setup(
    name='wikilabbook',
    version='0.1',
    description='Page generator for dokuwiki labbook',
    author='Nicolas Mielec',
    author_email='nicolas.mielec@gmail.com',
    packages=['wikilabbook'],
    install_requires=[
        'pyperclip',
        'future',
        'requests',
        'lxml',
        'pyyaml',
    ]
)
