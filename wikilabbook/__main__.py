import logging
import os

from .wikilabbook import main
from . import config


if __name__ == '__main__':
    logging.basicConfig(filename=os.path.join(config.USER_PATH, 'main.log'), level=logging.DEBUG)
    logging.info('Starting program' + '-'*15)
    main()
