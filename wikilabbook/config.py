import platform
import os
import yaml

# OS detection
if platform.system() == 'Windows':
    USER_PATH = os.path.join(os.environ['APPDATA'], 'wikilabbook')
elif platform.system() == 'Linux':
    USER_PATH = os.path.join(os.path.expanduser('~'), '.config', 'wikilabbook')
else:
    raise RuntimeError('Could not determine OS')

PACKAGE_PATH = os.path.dirname(os.path.realpath(__file__))

USER_TEMPLATE_PATH = os.path.join(USER_PATH, 'user_template.yaml')
DEFAULT_TEMPLATE_PATH = os.path.join(PACKAGE_PATH, 'default_template.yaml')

COOKIES_PATH = os.path.join(USER_PATH, 'dokuwiki.cookies')

user_template_exists = os.path.isfile(USER_TEMPLATE_PATH)

# If the template doesnt exist, it is copyed from the installation
# directory to the configuration directory
if not user_template_exists:
    import shutil
    try:
        os.makedirs(os.path.join(USER_PATH, 'default'))
    except (IOError, OSError):
        pass

    package_path = os.path.dirname(os.path.realpath(__file__))
    default_template_path_in_pkg = os.path.join(package_path, 'default_template.yaml')
    user_template_path_in_pkg = os.path.join(package_path, 'user_template.yaml')

    if not user_template_exists:
        shutil.copy(user_template_path_in_pkg, USER_TEMPLATE_PATH)
    print('Creating template in : \n{}'.format(USER_PATH))
    print('Please set the dokuwiki url and labbook path in the template.')

with open(DEFAULT_TEMPLATE_PATH, 'r') as template_file:
    TEMPLATE = yaml.safe_load(template_file)

with open(USER_TEMPLATE_PATH, 'r') as template_file:
    TEMPLATE.update(yaml.safe_load(template_file))
