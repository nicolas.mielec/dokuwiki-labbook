import argparse
import datetime
import logging

from .dokuwikiapi import DokuwikiAPIError, DokuwikiAPI
from . import pagegenerators as pg
from . import config

def get_labbook_dokuwikiapi():
    """ Returns DokuwikiAPI object connected to the labbook

    Tries to connect using the cookie file stored in the user folder.
    If not, prompts for credentials and saves the cookies.
    """
    wikiapi = DokuwikiAPI(
        wiki_url=config.TEMPLATE['WIKI_URL'],
        base_path=config.TEMPLATE['LABBOOK_PATH'],
    )
    try:
        wikiapi.load_cookies_from_pickle(config.COOKIES_PATH)
    except IOError:
        while not wikiapi.is_logged():
            try:
                wikiapi.login(prompt=True)
            except DokuwikiAPIError as e:
                print('Could not log in : {}.'.format(e))
                print('Ctrl+C to quit.')
            else:
                if config.TEMPLATE['SAVE_COOKIES']:
                    import pickle
                    with open(config.COOKIES_PATH, 'wb') as f:
                        pickle.dump(wikiapi.cookies, f)
                    print('Saved cookies file to {}.'.format(config.COOKIES_PATH))
    else:
        print('Connected using cookies.')

    return wikiapi

def set_base_path(path):
    """ Replaces the labbook base path in the user template by the given path
    """
    logger = logging.getLogger(__name__)

    path = path.replace('/', ':')
    while path[0] == ':':
        path = path[1:]
    while path[-1] == ':':
        path = path[:-1]

    with open(config.USER_TEMPLATE_PATH, 'r') as template_file:
        new_line = 'LABBOOK_PATH: {}\n'.format(path)
        new_lines = [
            new_line if 'LABBOOK_PATH' in line
            else line
            for line in template_file
        ]

    with open(config.USER_TEMPLATE_PATH, 'w') as template_file:
        template_file.writelines(new_lines)

    logger.info('Base path has been set to {}'.format(path))

def set_wiki_url(url):
    """ Replaces the wiki url in the user template by the given url
    """
    logger = logging.getLogger(__name__)

    while url[-1] == '/':
        url = url[:-1]
    url += '/'

    with open(config.USER_TEMPLATE_PATH, 'r') as template_file:
        new_line = 'WIKI_URL: {}\n'.format(url)
        new_lines = [
            new_line if 'WIKI_URL' in line
            else line
            for line in template_file
        ]
        lines = template_file.readlines()

    with open(config.USER_TEMPLATE_PATH, 'w') as template_file:
        template_file.writelines(new_lines)

    logger.info('Wiki url has been set to {}'.format(url))


def main():
    """
    Main program of the package called with python -m wikilabbook
    Generates pages for the labbook and creates them online

    See python -m wikilabbook -h for help
    """
    try:
        import pyperclip
    except ImportError:
        clip = False
    else:
        clip = True

    parser = argparse.ArgumentParser(prog='wikilabbook', description='Get dokuwiki labbook entry')
    parser.add_argument('--noclip', action='store_true', help='Do not store in clipboard')
    parser.add_argument('--file', '-f', help='Save to file', metavar='filename')
    parser.add_argument('--verbose', '-v', help='Print result to console', action='store_true')
    parser.add_argument('--set-path', help='Setup the labbook:path')
    parser.add_argument('--set-url', help='Setup the wiki url')
    parser.add_argument('--online', '-o', help='Create the page online', action='store_true')
    parser.add_argument('date', help='Date of the requested page', nargs='*', type=int)

    args = parser.parse_args()

    if args.set_path:
        set_base_path(args.set_path)
    elif args.set_url:
        set_wiki_url(args.set_url)
    else:
        if args.online:
            try:
                wikiapi = get_labbook_dokuwikiapi()
            except KeyboardInterrupt:
                exit()

        # Year page
        if len(args.date) == 1:
            page = pg.get_year_page(args.date[0])
            if args.online:
                date = datetime.datetime(year=args.date, month=1, day=1)
                try:
                    wikiapi.create_page('{date:%Y}'.format(date=date), content=page)
                except DokuwikiAPIError as e:
                    print('Could not create page : {}'.format(e))
                else:
                    print('Page created')

        # Month page
        if len(args.date) == 2:
            page = pg.get_month_page(args.date[0], args.date[1])
            if args.online:
                date = datetime.datetime(*args.date, day=1)
                try:
                    wikiapi.create_page('{date:%Y}/{date:%b%y}'.format(date=date), content=page)
                except DokuwikiAPIError as e:
                    print('Could not create page : {}'.format(e))
                else:
                    print('Page created')

        # Day page
        if len(args.date) == 3 or (not args.date):
            if not args.date:
                date = datetime.datetime.now()
            else:
                date = datetime.datetime(*args.date)
            page = pg.get_day_page(date.year, date.month, date.day)
            if args.online:
                try:
                    wikiapi.create_page('{date:%Y}:{date:%b%y}:{date:%Y_%m_%d}'.format(date=date), content=page)
                except DokuwikiAPIError as e:
                    print('Could not create page : {}'.format(e))
                else:
                    print('Page created')

        if args.file:
            with open(args.file, 'w') as output_file:
                output_file.write(page)
        if clip and not args.noclip:
            pyperclip.copy(page)
        if args.verbose:
            print('-----------Page content ----------------------------------')
            print(page)
            print('----------------------------------------------------------')