# Wikilabbook

This package can be used to generate pages in dokuwiki script for a basic labbook.
With the basic template, pages can be generated for years, months and days.

A year page will show links to every month.
A month page will show links to every day, and links to previous month, current year and next month.
A day page will show links to previous day, current month and next day.

The *default_template.yaml* in the package directory holds the default template of the pages.
The *user_template.yaml* located in the user folder contains other settings.
Settings and page templates set in the user template will be prefered to the default template file.

In order to be able to create a page directly on the wiki, when using the parameter `--online/ -o`, the program will ask credentials.
These are never stored on the computer.
Upon a successful login, a `cookiejar` from the `requests` `Session` object is saved in the user directory to be able to reconnect without asking for credentials again.

**To prevent cookies being saved, the `SAVE_COOKIES` parameter can be set to false in the user configuration file.**

## Installation :

Clone the repo or download a copy to a directory.
Then install with `python setup.py install` from the directory itself or as an editable package (so that it can be modified or updated easily) with `pip install -e path/to/package`.

On first use, the program will copy configuration files to a user directory :
`%APPDATA%\wikilabbook\` on windows, `~/user/.config/wikilabbook/` on linux.

**After installation**, modify the dokuwiki url using the arguments `--set-url http://url/of/the/dokuwiki` and the labbook path with arguments `--set-path path:to:the:wiki`.
Example dokuwiki url is : `https://syrte-int.obspm.fr/dokuwiki`, and labbook path : `wiki:tfc:prive:gyro:cavity:labbook` (there should be no `:` at the end of the path).

## Usage
Get a dokuwiki labbook page template

```
positional arguments:
  date                  Date of the requested page

optional arguments:
  -h, --help            show this help message and exit
  --noclip              Do not store in clipboard
  --file filename, -f filename 
                        Save to file
  --verbose, -v         Print result to console
  --online, -o          Create the page online
  --set-url             Set the dokuwiki url
  --set-path            Set the labbook path
```
 
 
## Examples : 
* Get today's page : `python -m wikilabbook`   
The page of the day will get copied to the clipboard

* Get a year page : `python -m wikilabbook 1992`   
The 1992 year page will be created

* Get a month page : `python -m wikilabbook 1992 5`   
The May 1992 page is copied to the clipboard

* Create the page on the dokuwiki : `python -m wikilabbook 1992 5 -o`    
The May 1992 page is copied to the clipboard and created on the wiki.  
Login credentials will be prompted and saved by default (see `SAVE_COOKIES`).

* To save the page to a file : `python -m wikilabbook 1992 5 6 -f page.txt`    
The May 6th 1992 page is saved to page.txt and copied to the clipboard

* To see the output in the console : `python -m wikilabbook 1992 5 6 -v`    
The May 6th 1992 page is printed on the console and copied to the clipboard

* To not copy to clipboard : `python -m wikilabbook 1992 5 6 -v --noclip`    
The May 6th 1992 page is only printed on the console

Options can be combined : `python -m wikilabbook 1992 5 6 -v --noclip -f whosbirthday.txt`   
will output the May 6th 1992 page to the console and to the file *whosbirthday.txt* only.


### Example page :

On Friday January 12th the command `python -m wikilabook` will copy this text to the clipboard
```
====== Friday 2018-01-12 ======
^[[wiki:tfc:prive:gyro:cavity:labbook:2018:Jan18:2018_01_11|Thursday 2018-01-11]] ^ [[wiki:tfc:prive:gyro:cavity:labbook:2018:Jan18|January 2018]] ^ [[wiki:tfc:prive:gyro:cavity:labbook:2018:Jan18:2018_01_15|Monday 2018-01-15]]^

```